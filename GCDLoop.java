public class GCDLoop {
    public static void main(String[] args) {
        int num1 = Integer.parseInt(args[0]);
        int num2 = Integer.parseInt(args[1]);
        int cont;
        // if first input is smaller than second, my algorithm will collapse!
        if (num2 > num1) {
            cont = num2;
            num2 = num1;
            num1 = cont;
        }
        int temp;
        while (num1 % num2 != 0) {
            temp = num1;
            num1 = num2;
            num2 = temp % num2;
        }
        System.out.println(num2);
    }
}