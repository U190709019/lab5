import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FindSequence {

	public static void main(String[] args) throws FileNotFoundException{
		int matrix[][] = readMatrix();
		int koord[][] = {{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0}}; //for reverse I will store point datas here
		int f;
		boolean found = false;
search: for (int i=0; i < matrix.length; i++) {
			for (int j=0; j < matrix[i].length; j++) {
				f = 0;
				if (search(0,matrix, i,j, koord, f)){
					found = true;
					break search;
				}
			}
		}
		for (int m = 0; m < koord.length; m++) {
			System.out.println("Before reverse coordinat "+ m +": (" + koord[m][0] + "," + koord[m][1] +")");
		}
		if (found) {
			System.out.println("A sequence is found");
		}
		printMatrix(matrix);

	}

	private static boolean search (int number, int[][]matrix, int row, int col, int[][]koord, int f) {
		if (matrix[row][col] == number) {
			koord[f][0] = row;
			koord[f][1] = col;
			return search(number+1,matrix,row,col,koord,f+1);
		}
		else if (row-1>=0 && matrix[row-1][col]==number) {
			koord[f][0] = row-1;
			koord[f][1] = col;
			return search(number+1, matrix,row-1,col,koord,f+1);
		}
		else if (col+1<=9 && matrix[row][col+1]==number) {
			koord[f][0] = row;
			koord[f][1] = col+1;
			return search(number+1, matrix, row, col+1,koord,f+1);
		}
		else if (row+1<=9 && matrix[row+1][col]==number) {
			koord[f][0] = row+1;
			koord[f][1] = col;
			return search(number+1, matrix, row+1, col, koord, f+1);
		}
		else if (col-1>=0 && matrix[row][col-1]==number) {
			koord[f][0] = row;
			koord[f][1] = col-1;
			return search(number+1, matrix, row, col-1, koord, f+1);
		}

		if (number==10) {
			for (int r = 0, m = 9; r < koord.length; r++, m--) {
				matrix[koord[r][0]][koord[r][1]] = m;
			}
			return true;
		}
		return false;

	}

	private static int[][] readMatrix() throws FileNotFoundException{
		int[][] matrix = new int[10][10];
		File file = new File("matrix.txt");

		try (Scanner sc = new Scanner(file)){

			
			int i = 0;
			int j = 0;
			while (sc.hasNextLine()) {
				int number = sc.nextInt();
				matrix[i][j] = number;
				if (j == 9)
					i++;
				j = (j + 1) % 10;
				if (i == 10)
					break;
			}
		} catch (FileNotFoundException e) {
			throw e;
		}
		return matrix;
	}
	
	private static void printMatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
					System.out.print(matrix[i][j]+" ");
			}
			System.out.println();
		}	
	}
}
