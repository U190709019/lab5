public class GCDRec {

    public static int myRecursion(int num1, int num2) {
        int cont;
        if (num1 < num2) {
            cont = num1;
            num1 = num2;
            num2 = cont;
        }
        int temp;
        if (num1 % num2 != 0) {
            temp = num1 % num2;
            num1 = num2;
            num2 = temp;
            return myRecursion(num1, num2);
        } else {
            return num2;
        }
    }

    public static void main(String[] args) {
        int num1 = Integer.parseInt(args[0]);
        int num2 = Integer.parseInt(args[1]);
        System.out.println(myRecursion(num1, num2));
    }
}